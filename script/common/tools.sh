#!/bin/bash
#
# Setup the the box. This runs as root

apt-get -y update
apt-get -y install curl

# Remove configuration-management systems preinstalled in official Ubuntu images
apt-get -y remove --purge chef chef-zero puppet puppet-common landscape-client landscape-common
# And any dependencies
apt-get -y autoremove

# add the PostgreSQL repository
echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" > /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Get any security updates not in the base image
sudo apt-get update
sudo apt-get -y dist-upgrade

# Other packages we need
sudo apt-get install -q -y git vim nodejs postgresql-9.4 libpq-dev redis-server nginx

# install the heroku toolbelt
wget -O- https://toolbelt.heroku.com/install-ubuntu.sh | sh

# Copy our files into place
sudo rsync -rtv /tmp/files/etcfiles/ /etc
rsync -rtv /tmp/files/binfiles/ /usr/local/bin
# Force MOTD generation (will only work on 14.04)
rm -f /etc/update-motd.d/51-cloudguest
run-parts --lsbsysinit /etc/update-motd.d > /run/motd.dynamic

# update the ownership of the postgres files
chown -R postgres /etc/postgresql/9.4/main/

service postgresql restart

# install the following as root
su -l vagrant <<'EOF'
# Install RVM
gpg --yes --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable
source ~/.rvm/scripts/rvm

# ignore documentation
echo "install: --no-rdoc --no-ri" >> ~/.gemrc 
echo "update:  --no-rdoc --no-ri" >> ~/.gemrc

# install the rails 
rvm install 2.2.2
rvm use 2.2.2@global --default
gem install bundler
gem install rails

# create a empty rails app so we can install the gems
mkdir -p ~/tmp
rails new TempApp ~/tmp -d postgresql
rm -rf ~/tmp
rm -rf ~/TempApp

# Copy files that should be owned by the user account
rsync -rtv /tmp/files/dotfiles/ /home/vagrant

# Our bash setup will cd to workspace on login.
ln -s /vagrant ~/workspace
EOF

# You can install anything you need here.
